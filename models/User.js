var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var user = new Schema({
    name: {type: String, required: true, trim: true},
    phoneNumber:{type:String, required:true,index:{unique:true}},
    password:{type:String,required:true}
});

const User = mongoose.model('User', user);
module.exports = User;