// Import FirebaseAuth and firebase.
import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase';
import Container from "@material-ui/core/Container";
// Configure Firebase.
const config = {
    apiKey: "AIzaSyDPiu2xnNVPhQ9TSZYz7vOAKAm_xdKcmzU",
    authDomain: "office-manager-206c8.firebaseapp.com",
    databaseURL: "https://office-manager-206c8.firebaseio.com",
    projectId: "office-manager-206c8",
    storageBucket: "office-manager-206c8.appspot.com",
    messagingSenderId: "262138729060",
    appId: "1:262138729060:web:072fc8ab72bdecef170571",
    measurementId: "G-T5KN5BMPEX"
};
export const instance = firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    signInSuccessUrl: '/signup',
    signInOptions: [
        {
            provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
            // The default selected country.
            defaultCountry: 'BD'
        }
    ]
};

class SignInScreen extends React.Component {
    render() {
        return (
            <div>
                <h1>My App</h1>
                <Container><h3>To SignUp Please verify you mobile number</h3></Container>
                <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>
            </div>
        );
    }
}

export default SignInScreen;