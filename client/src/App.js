import React from 'react';
import './App.css';
import SignIn from "./pages/SignInSide";
import ButtonAppBar from "./components/Appbar";
import SignUp from "./pages/SignUp";
import SignInScreen from "./pages/SignInScreen";
import dashboard from './pages/Dashboard'
import {BrowserRouter, Route, Switch} from 'react-router-dom';


function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={SignIn} />
                <Route path='/signup' component={SignUp}/>
                <Route path='/signup_verify' component={SignInScreen}/>
                <Route path='/dashboard' component={dashboard}/>
            </Switch>
        <ButtonAppBar/>
        </BrowserRouter>
    </div>
  );
}

export default App;
