const signupvalidator = require("../validator/signupValidator");
const loginvalidator = require("../validator/loginValidator");
const User = require("../models/User");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {serverError, resourceError} = require('../util/error');

module.exports = {
    refreshToken(req, res){
        const refreshToken = req.body.token;
        if (refreshToken == null) return res.sendStatus(401)
        if (!refreshToken.includes(refreshToken)) return res.sendStatus(403)
        jwt.verify(refreshToken, process.env.REFRESH_SECRET, (err, user) => {
            if (err) return res.sendStatus(403)
            {
                let token = jwt.sign({
                    _id: user._id,
                    name: user.name
                }, process.env.SECRET, {expiresIn: '10m'});
                res.json({
                    token: `Bearer ${token}`
                })
            }
    })
},
    login(req, res) {
        let { phoneNumber, password} = req.body;
        let validator = loginvalidator({phoneNumber, password});

        if (!validator.isValid) {
            return res.status(400).json(validator.error);
        } else {
            User.findOne({ phoneNumber })
                .then(user => {
                    if (!user) {
                        return resourceError(res, '404')
                    }
                    bcrypt.compare(password, user.password, (err, result) => {
                        if (err) {
                            return serverError(res, err)
                        }
                        if (!result) {
                            return resourceError(res, 'Password Doesn\'t Match')
                        }

                        let token = jwt.sign({
                            _id: user._id,
                            name: user.name
                        }, process.env.SECRET, {expiresIn: '10m'});

                        let refreshToken = jwt.sign({
                            _id: user._id,
                            name: user.name
                        }, process.env.REFRESH_SECRET, {expiresIn: '1d'});

                        res.status(200).json({
                            status: '200',
                            token: `Bearer ${token}`,
                            refreshToken: `Bearer ${refreshToken}`
                        })

                    })
                })

                .catch(error => serverError(res, error))
        }
    },
    signup(req, res) {
        let {name, phoneNumber, password, confirmPassword} = req.body;
        let validation = signupvalidator({name, phoneNumber, password, confirmPassword});

        if (!validation.isValid) {
            return res.status(400).json(validation.error);
        } else {
            User.findOne({ phoneNumber })
                .then(user => {
                    if (user) {
                        return resourceError(res, 'Number Already Exist')
                    }

                    bcrypt.hash(password, 11, (err, hash) => {
                        if (err) {
                            return resourceError(res, 'Error Occurred in server')
                        }

                        let user = new User({
                            name,
                            phoneNumber,
                            password: hash
                        });

                        user.save()
                            .then(user => {
                                res.status(201).json({
                                    status: '201',
                                    user
                                })
                            })
                            .catch(error => serverError(res, error))
                    })
                })
                .catch(error => serverError(res, error))
        }
    },
    allUser(req, res) {
        User.find()
            .then(users => {
                res.status(200).json(users)
            })
            .catch(error => serverError(res, error))
    }
};