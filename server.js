const express = require("express");
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const signRoute = require('./routes/userRoute');
const dashboard = require('./routes/dashBoard');
const passport = require('passport');
require('dotenv').config();

app.use(bodyparser.urlencoded({extended : false}));
app.use(bodyparser.json());
app.use(morgan('dev'));
app.use(cors());




app.use('/user',signRoute);
app.use(passport.initialize());
require('./passport')(passport);
app.use('/dashboard',dashboard);
app.get('/',(req, res)=>
res.json({
    message: 'Welcome to backend'
})
);

const PORT = process.env.PORT || 4000;

app.listen(PORT, () =>
    {
        //mongodb+srv://shafayetrahat:pegasus84@office-manager-vaa4v.gcp.mongodb.net/test?retryWrites=true&w=majority
        console.log('Server is running at: '+PORT);
        mongoose.connect(process.env.MONGO_CONNECTION,
            { useNewUrlParser: true , useUnifiedTopology: true },
            () =>
            {
                console.log('Database connected....')
            }
            )
    }
);