// const validator = require('validator');

const validate = user => {
    let error = {};

    if (!user.phoneNumber) {
        error.phoneNumber = 'Please Provide Your Phone Number'
    }
    if (!user.password) {
        error.password = 'Please Provide a Password'
    } else if (user.password.length < 6) {
        error.password = 'Password Must be Greater or Equal 6 Character'
    }
    return {
        error,
        isValid: Object.keys(error).length === 0
    }
};

module.exports = validate;