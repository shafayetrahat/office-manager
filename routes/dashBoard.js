const router = require('express').Router();
const dashboardmiddleware = require('../middleware/authenticate');
const { stat } = require('../controllers/dashboardController');

router.get('/stat', dashboardmiddleware, stat);

module.exports = router;