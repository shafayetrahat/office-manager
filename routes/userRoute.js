const router = require('express').Router();
const {login, signup, refreshToken} = require('../controllers/userController');


router.post('/signup', signup);
router.post('/login',login);
router.post('/LGHFpnzrfSJpj/QLgExgJ7WcIFaiJHNJdMfRZOAmt6g=',refreshToken);

module.exports = router;